QT       += core gui websockets
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = marbot
TEMPLATE = app

SOURCES  += marbot.cpp utama.cpp
HEADERS  += marbot.hpp
FORMS    += marbot.ui \
    normal.ui \
    penuh.ui
RC_FILE  += app.rc
