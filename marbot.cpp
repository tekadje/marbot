#include "marbot.hpp"
#include "ui_marbot.h"
#include "ui_normal.h"
#include "ui_penuh.h"
#include <QWebSocketServer>
#include <QWebSocket>
#include <QSslCertificate>
#include <QSslKey>

Marbot::Marbot(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Marbot),
    un(new Ui::Normal),
    up(new Ui::Penuh),
    ikon(new QSystemTrayIcon),
    konfigurasi(new QSettings("app/konfigurasi.ini", QSettings::IniFormat)),
    m_pWebSocketServer(Q_NULLPTR),
    m_clients()
{
    // Inisialisasi Desain Antarmuka
    ui->setupUi(this);
    ui->areaInformasi->showMessage("Copyleft © 2016, Hendra Dwi Saputra.");
    ikon->setIcon(QIcon(QString::fromUtf8("app/lgo/server.png")));
    ikon->setVisible(true);
    connect(ikon, &QSystemTrayIcon::activated, this, &Marbot::ikonAktif);
    lebar = QApplication::desktop()->width();
    tinggi = QApplication::desktop()->height();
    modeNormal();
}

Marbot::~Marbot()
{
    delete ikon;
    delete up;
    delete un;
    delete ui;
}

void Marbot::modeNormal()
{
    // Menghapus seluruh item di rancangan
    QLayoutItem *child;
    while((child = ui->rancangan->takeAt(0)) != 0)
    {
        if(child->widget() != NULL)
        {
            delete (child->widget());
        }
        delete child;
    }

    // Menciptakan widget baru yang ditanamkan antarmuka ui
    QWidget *normal = new QWidget();
    un->setupUi(normal);

    // Tambahkan aksesoris dan pengaturan ke antarmuka
    if(QFile("app/konfigurasi.ini").exists())
    {
        if(!konfigurasi->value("Direktori").isNull())
        {
            un->txtDirektori->setText(konfigurasi->value("Direktori").toString());
        }
        if(!konfigurasi->value("Port").isNull())
        {
            un->txtPort->setValue(konfigurasi->value("Port").toInt());
        }
    }
    un->btnCari->setIcon(QIcon(QString::fromUtf8("app/img/Search.png")));
    if(serverMulai)
    {
        un->btnMulai->setIcon(QIcon(QString::fromUtf8("app/img/Cancel.png")));
    }
    else
    {
        un->btnMulai->setIcon(QIcon(QString::fromUtf8("app/img/Start.png")));
    }
    un->btnKeluar->setIcon(QIcon(QString::fromUtf8("app/img/Standby.png")));

    // Sinyal dan Slot
    connect(un->btnCari, &QPushButton::clicked, this, &Marbot::aturDirektori);
    connect(un->btnMulai, &QPushButton::clicked, this, &Marbot::administrasiServer);
    connect(un->btnKeluar, &QPushButton::clicked, this, &Marbot::tutupAplikasi);

    // Menambahkan widget ke rancangan agar terlihat
    ui->rancangan->addWidget(normal);
    move(lebar-320-20, tinggi-this->height()*2);
}

void Marbot::modePenuh()
{
    // Menghapus seluruh item di rancangan
    QLayoutItem *child;
    while((child = ui->rancangan->takeAt(0)) != 0)
    {
        if(child->widget() != NULL)
        {
            delete (child->widget());
        }
        delete child;
    }

    // Menciptakan widget baru yang ditanamkan antarmuka ui
    QWidget *penuh = new QWidget();
    up->setupUi(penuh);
    un->setupUi(up->panel);

    // Tambahkan aksesoris dan pengaturan ke antarmuka
    if(QFile("app/konfigurasi.ini").exists())
    {
        if(!konfigurasi->value("Direktori").isNull())
        {
            un->txtDirektori->setText(konfigurasi->value("Direktori").toString());
        }
        if(!konfigurasi->value("Port").isNull())
        {
            un->txtPort->setValue(konfigurasi->value("Port").toInt());
        }
    }
    un->btnCari->setIcon(QIcon(QString::fromUtf8("app/img/Search.png")));
    if(serverMulai)
    {
        un->btnMulai->setIcon(QIcon(QString::fromUtf8("app/img/Cancel.png")));
    }
    else
    {
        un->btnMulai->setIcon(QIcon(QString::fromUtf8("app/img/Start.png")));
    }
    un->btnKeluar->setIcon(QIcon(QString::fromUtf8("app/img/Standby.png")));

    // Sinyal dan Slot
    connect(un->btnCari, &QPushButton::clicked, this, &Marbot::aturDirektori);
    connect(un->btnMulai, &QPushButton::clicked, this, &Marbot::administrasiServer);
    connect(un->btnKeluar, &QPushButton::clicked, this, &Marbot::tutupAplikasi);

    // Menambahkan widget ke rancangan agar terlihat
    ui->rancangan->addWidget(penuh);
}

void Marbot::changeEvent(QEvent *e)
{
    if(e->type() == QEvent::WindowStateChange)
    {
        QWindowStateChangeEvent *event = static_cast<QWindowStateChangeEvent* >(e);
        if(event->oldState() && Qt::WindowMinimized)
        {
            modeNormal();
        }
        else if(event->oldState() == Qt::WindowNoState && this->windowState() == Qt::WindowMaximized)
        {
            modePenuh();
        }
    }
}

void Marbot::closeEvent(QCloseEvent *event)
{
    event->ignore();
    hide();
}

void Marbot::aturDirektori()
{
    dir = QFileDialog::getExistingDirectory(this, tr("Pilih direktori"), QDir::currentPath(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(!dir.isEmpty())
    {
        un->txtDirektori->setText(dir);
        konfigurasi->setValue("Direktori", dir);
        konfigurasi->sync();
    }
}

void Marbot::administrasiServer()
{
    if(serverMulai && m_pWebSocketServer->isListening())
    {
        un->btnMulai->setIcon(QIcon(QString::fromUtf8("app/img/Start.png")));
        serverMulai = false;
        akhiriServer();
    }
    else
    {
        un->btnMulai->setIcon(QIcon(QString::fromUtf8("app/img/Cancel.png")));
        serverMulai = true;
        mulaiServer();
    }
}

void Marbot::tutupAplikasi()
{
    // Menyimpan pengaturan direktori data aplikasi
    if(!un->txtDirektori->text().isEmpty())
    {
        konfigurasi->setValue("Direktori", un->txtDirektori->text());
        konfigurasi->sync();
    }

    // Menyimpan pengaturan port server aplikasi
    if(un->txtPort->value() >= 1111 && un->txtPort->value() <= 9999)
    {
        konfigurasi->setValue("Port", un->txtPort->value());
        konfigurasi->sync();
    }

    // Menutup aplikasi apabila server sedang tidak dijalankan
    if(serverMulai && m_pWebSocketServer->isListening())
    {
        ui->areaInformasi->showMessage("Server masih berjalan, harap akhiri server terlebih dahulu.");
    }
    else
    {
        qApp->quit();
    }
}

void Marbot::ikonAktif(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
        if(this->isHidden())
        {
            this->show();
        }
        else
        {
            this->hide();
        }
        break;
    default:
        break;
    }
}

void Marbot::mulaiServer()
{
    // Websocket Initialition
    m_pWebSocketServer = new QWebSocketServer(QStringLiteral("Sistem Tutor Cerdas"),
                                              QWebSocketServer::SecureMode,
                                              this);
    QSslConfiguration sslConfiguration;
    QFile certFile(QStringLiteral("app/crt/localhost.crt"));
    QFile keyFile(QStringLiteral("app/crt/localhost.key"));
    certFile.open(QIODevice::ReadOnly);
    keyFile.open(QIODevice::ReadOnly);
    QSslCertificate certificate(&certFile, QSsl::Pem);
    QSslKey sslKey(&keyFile, QSsl::Rsa, QSsl::Pem);
    certFile.close();
    keyFile.close();
    sslConfiguration.setPeerVerifyMode(QSslSocket::VerifyNone);
    sslConfiguration.setLocalCertificate(certificate);
    sslConfiguration.setPrivateKey(sslKey);
    sslConfiguration.setProtocol(QSsl::TlsV1SslV3);
    m_pWebSocketServer->setSslConfiguration(sslConfiguration);

    if (m_pWebSocketServer->listen(QHostAddress::Any, un->txtPort->value()))
    {
        ui->areaInformasi->showMessage(QString("Server mulai melayani di port ").append(un->txtPort->text()));
        connect(m_pWebSocketServer, &QWebSocketServer::newConnection, this, &Marbot::onNewConnection);
        connect(m_pWebSocketServer, &QWebSocketServer::sslErrors, this, &Marbot::onSslErrors);
    }
}

void Marbot::akhiriServer()
{
    m_pWebSocketServer->close();
    qDeleteAll(m_clients.begin(), m_clients.end());
    ui->areaInformasi->showMessage("Server berhenti melayani.");
}

void Marbot::onNewConnection()
{
    QWebSocket *pSocket = m_pWebSocketServer->nextPendingConnection();
    ui->areaInformasi->showMessage(QString("Klien tersambung:").append(pSocket->peerAddress().toString()));
    connect(pSocket, &QWebSocket::textMessageReceived, this, &Marbot::processTextMessage);
    connect(pSocket, &QWebSocket::binaryMessageReceived, this, &Marbot::processBinaryMessage);
    connect(pSocket, &QWebSocket::disconnected, this, &Marbot::socketDisconnected);
    m_clients << pSocket;
}

void Marbot::processTextMessage(QString message)
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    if (pClient)
    {
        pClient->sendTextMessage(message);
    }
}

void Marbot::processBinaryMessage(QByteArray message)
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    if (pClient)
    {
        pClient->sendBinaryMessage(message);
    }
}

void Marbot::socketDisconnected()
{
    ui->areaInformasi->showMessage("Klien terputus");
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    if (pClient)
    {
        m_clients.removeAll(pClient);
        pClient->deleteLater();
    }
}

void Marbot::onSslErrors(const QList<QSslError> &)
{
    ui->areaInformasi->showMessage("Kesalahan ssl terjadi");
}
