#ifndef MARBOT_HPP
#define MARBOT_HPP

#include <QMainWindow>
#include <QWindowStateChangeEvent>
#include <QSystemTrayIcon>
#include <QDesktopWidget>
#include <QFileDialog>
#include <QSettings>
#include <QSslError>

QT_FORWARD_DECLARE_CLASS(QWebSocketServer)
QT_FORWARD_DECLARE_CLASS(QWebSocket)

namespace Ui {
class Marbot;
class Normal;
class Penuh;
}

class Marbot : public QMainWindow
{
    Q_OBJECT

public:
    explicit Marbot(QWidget *parent = 0);
    ~Marbot();
    void modeNormal();
    void modePenuh();

protected:
    void changeEvent(QEvent *e);
    void closeEvent(QCloseEvent *event);

private slots:
    void aturDirektori();
    void administrasiServer();
    void tutupAplikasi();
    void ikonAktif(QSystemTrayIcon::ActivationReason reason);

private Q_SLOTS:
    void onNewConnection();
    void processTextMessage(QString message);
    void processBinaryMessage(QByteArray message);
    void socketDisconnected();
    void onSslErrors(const QList<QSslError> &errors);

private:
    void mulaiServer();
    void akhiriServer();
    Ui::Marbot *ui;
    Ui::Normal *un;
    Ui::Penuh *up;
    int lebar;
    int tinggi;
    bool serverMulai = false;
    QSystemTrayIcon *ikon;
    QSettings *konfigurasi;
    QString dir;
    QWebSocketServer *m_pWebSocketServer;
    QList<QWebSocket *> m_clients;
};

#endif // MARBOT_HPP
