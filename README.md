# MARBOT versi 1.1.0 alpha #

Aplikasi ini adalah aplikasi Sistem Tutor Cerdas (STC) yang menggunakan Qt Framework dan Teknologi WebSocket. Aplikasi ini masih tahap pengembangan, namun fungsi dasar websocket telah berjalan dengan sempurna. Jika anda tertarik dengan teknologi websocket di Qt, maka aplikasi ini akan menjadi referensi menarik bagi anda.

### Apa yang ada di repositori ini? ###

* Ringkasan Cepat
* Versi Pengembangan
* Utilitas Pendukung
* Sertifikat Lokal
* Ikon Logo

### Bagaimana saya menggunakannya? ###

* Aplikasi dibangun dengan Qt Creator di Windows7
* Dependensi Qt Library 5.6.0 ke atas

### Tatacara Kontribusi ###

* Menulisi aplikasi tes
* Mengulas Kode
* Mendesain ulang antarmuka dan prosedur

### Jika ingin berbincang dan berbagi? ###

* Pemilik Repo atau Admin
* Hendra Dwi Saputra (tekadje@hotmail.com)