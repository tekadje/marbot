#include "marbot.hpp"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    Marbot m;
    m.show();
    return app.exec();
}
